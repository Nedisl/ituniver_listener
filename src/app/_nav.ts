export const navItems = [
  {
    name: 'Профиль',
    url: '/dashboard',
    icon: 'icon-user'
  },
  {
    title: true,
    name: 'Настройки'
  },
  {
    name: 'Безопасность',
    url: '/settings/security',
    icon: 'icon-lock'
  },
  {
    name: 'Профиль',
    url: '/settings/profile',
    icon: 'icon-user'
  },
  {
    title: true,
    name: 'Курсы'
  },
  {
    name: 'Скоро',
    url: '/courses/soon',
    icon: 'icon-book-open'
  },
  {
    name: 'Каталог',
    url: '/courses/catalog',
    icon: 'icon-book-open'
  },
  {
    name: 'Мои курсы',
    url: '/courses/my',
    icon: 'icon-book-open'
  },
  {
    name: 'Подписки',
    url: '/courses/subscriptions',
    icon: 'icon-book-open'
  }
];
