import {Injectable} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {RestApiService} from './rest-api.service';
import {ToastData, ToastOptions, ToastyConfig, ToastyService} from 'ngx-toasty';
import {BehaviorSubject} from 'rxjs';
import {Title} from '@angular/platform-browser';


@Injectable({providedIn: 'root'})
export class DataService {
  message = '';
  user: any = null;
  cities: Array<any>;
  universities: Array<any>;

  observableUser = new BehaviorSubject(null);

  constructor(private router: Router, private rest: RestApiService, private toastyService: ToastyService,
              private toastyConfig: ToastyConfig, private titleService: Title) {

    this
      .rest
      .getAllCities()
      .then(data => {
        if (data['meta'].success) {
          this.cities = data['data'].cities;
          console.log(this.cities);
        }
      })
      .catch(err => console.log(err));

    this
      .rest
      .getAllUniversities()
      .then(data => {
        if (data['meta'].success) {
          this.universities = data['data'].universities;
        }
      })
      .catch(err => console.log(err));
  }

  setTitle(title: string) {
    this.titleService.setTitle(title + ' - IT Univer');
  }

  addToast(title: string, message: string, type: string) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: message,
      showClose: true,
      timeout: 5000,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been removed!');
      }
    };
    switch (type) {
      case 'info':
        this.toastyService.info(toastOptions);
        break;
      case 'success':
        this.toastyService.success(toastOptions);
        break;
      case 'wait':
        this.toastyService.wait(toastOptions);
        break;
      case 'error':
        this.toastyService.error(toastOptions);
        break;
      case 'warning':
        this.toastyService.warning(toastOptions);
        break;
    }
  }

  error(message) {
    this.addToast('Ошибка', message, 'error');
  }

  success(message) {
    this.addToast(message, '', 'success');
  }

  warning(message) {
    this.addToast('Внимание', message, 'warning');
  }

  async getListenerProfile() {
    try {
      if (localStorage.getItem('listener_token')) {
        const profile_data = await this
          .rest
          .getListenerProfile();
        this.user = profile_data['data'].user;
        console.log(this.user);
        this.observableUser.next(this.user);
      }
    } catch (error) {
      if (error.error.meta) {
        this.clearProfile();
        await this.router.navigate(['login']);
      }
    }
  }
  async getCuratorProfile() {
    try {
      if (localStorage.getItem('listener_token')) {
        const profile_data = await this
          .rest
          .getListenerProfile();
        this.user = profile_data['data'].user;
        console.log(this.user);
        this.observableUser.next(this.user);
      }
    } catch (error) {
      if (error.error.meta) {
        this.clearProfile();
        await this.router.navigate(['login']);
      }
    }
  }

  clearProfile() {
    this.user = null;
    localStorage.clear();
  }
}
