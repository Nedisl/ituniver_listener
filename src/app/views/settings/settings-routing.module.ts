import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecurityComponent } from './security.component';
import { ProfileComponent } from './profile.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Настройки'
    },
    children: [
      {
        path: 'security',
        component: SecurityComponent,
        data: {
          title: 'Безопасность'
        }
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {
          title: 'Профиль'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {}
