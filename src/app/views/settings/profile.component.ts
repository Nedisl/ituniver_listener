import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {UploadFileService} from '../../upload-file.service';
import {NgxSpinnerService} from 'ngx-spinner';


@Component({
  templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit{
  universityId: number;
  cityId: number;
  firstName: string;
  lastName: string;
  fatherName: string;
  email: string;
  phone: string;
  avatar: string;
  speciality: string;
  course: number;
  submitDisabled: boolean;

  isEmailValid: boolean;
  isNameCorrect: boolean;
  isPhoneCorrect: boolean;
  isUniversityCorrect: boolean;
  isCityCorrect: boolean;
  allCorrect: boolean;

  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private fileUploader: UploadFileService,
               private spinner: NgxSpinnerService) {}
  async ngOnInit() {
    this.isEmailValid = true;
    this.isNameCorrect = true;
    this.isPhoneCorrect = true;
    this.isUniversityCorrect = true;
    this.isCityCorrect = true;
    this.allCorrect = true;
    this.firstName = '';
    this.lastName  = '';
    this.fatherName = '';
    this.email = '';
    this.phone = '';

    await this.spinner.show();
    await this.data.getListenerProfile();
    await this.spinner.hide();
    this.universityId = this.data.user.university_id;
    this.cityId = this.data.user.city_id;
    this.avatar = this.data.user.avatar;
    this.speciality = this.data.user.speciality;
    this.fatherName = this.data.user.father_name;
    this.firstName = this.data.user.first_name;
    this.lastName = this.data.user.last_name;
    this.phone = this.data.user.phone;
    this.email = this.data.user.email;
    this.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + this.data.user.avatar;
    this.course = (+this.course === 0) ?  '' : this.data.user.course;
  }

  async update() {
    this.submitDisabled = true;
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid =  re.test(String(this.email).toLowerCase());
    this.isNameCorrect = (this.firstName !== '' && this.lastName !== '' && this.fatherName !== '');
    this.isPhoneCorrect = (this.phone.length === 10);
    this.isUniversityCorrect = (this.universityId !== 0);
    this.isCityCorrect = (this.cityId !== 0);
    this.submitDisabled = true;
    // tslint:disable-next-line:max-line-length
    this.allCorrect = this.isEmailValid && this.isNameCorrect && this.isPhoneCorrect && this.isUniversityCorrect && this.isCityCorrect;
    if (this.allCorrect) {
      try {
        if (this.universityId !== 0) {
          await this
            .rest
            .updateListener({
              first_name: this.firstName,
              last_name: this.lastName,
              father_name: this.fatherName,
              email: this.email,
              phone: this.phone,
              university_id: this.universityId,
              city_id: this.cityId,
              speciality: this.speciality,
              course: this.course
            }).then((res) => {
              console.log(res);
              this
                .data
                .addToast('Данные успешно обновлены', '', 'success');
              this
                .router
                .navigate(['/dashboard']);
            });

        } else {
          this
            .data
            .addToast('Выберите университет!', '', 'error');
        }
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }

  async fileChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadImage(file);

      try {
        await this
          .rest
          .updateAvatar({
            avatar: pictureUrl
          });
        this.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.data.user.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this
          .data
          .success('Аватар обновлен');

        await this
          .data
          .getListenerProfile();

      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }

}
