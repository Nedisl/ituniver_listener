import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { DataService } from '../../data.service';
import { RestApiService } from '../../rest-api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  avatar: string;

  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    await this.spinner.show();
    this.data.getListenerProfile().then(result => {
      if (this.data.user) {
        this.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + this.data.user.avatar;
      }
      this.spinner.hide();
    });
    if (localStorage.getItem('listener_token') === null) {
      this
        .router
        .navigate(['/login']);
    }
  }
}
