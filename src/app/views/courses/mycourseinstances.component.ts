import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  templateUrl: 'mycourseinstances.component.html'
})

export class MycourseinstancesComponent implements OnInit {
  courseInstances: any = null;
  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    if (this.courseInstances == null) {
      await this.spinner.show();
    }

    await this.data.getCuratorProfile();
    this.courseInstances = await this.rest.getListenerCourseInstancesByStatus();
    await this.spinner.hide();
  }
}

