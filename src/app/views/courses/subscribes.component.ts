import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  templateUrl: 'subscribes.component.html'
})

export class SubscribesComponent implements OnInit {
  courses: any = null;
  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    if (this.courses == null) {
      await this.spinner.show();
    }

    await this.data.getCuratorProfile();
    this.courses = await this.rest.getUserSubscriptions();
    console.log(this.courses);
    await this.spinner.hide();
  }
}

