import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list.component';
import { MycourseinstancesComponent } from './mycourseinstances.component';
import {CatalogComponent} from './catalog.component';
import {SubscribesComponent} from './subscribes.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Курсы'
    },
    children: [
      {
        path: 'soon',
        component: ListComponent,
        data: {
          title: 'Скоро'
        }
      },
      {
        path: 'my',
        component: MycourseinstancesComponent,
        data: {
          title: 'Мои курсы'
        }
      },
      {
        path: 'subscriptions',
        component: SubscribesComponent,
        data: {
          title: 'Подписки'
        }
      },
      {
        path: 'catalog',
        component: CatalogComponent,
        data: {
          title: 'Мои курсы'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {}
