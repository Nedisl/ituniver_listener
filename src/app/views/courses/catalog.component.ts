import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  templateUrl: 'catalog.component.html'
})


export class CatalogComponent implements OnInit {
  courses: any = null;
  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {}

  async ngOnInit() {
    if (this.courses == null) {
      await this.spinner.show();
    }

    await this.data.getListenerProfile();
    this.courses = await this.rest.getCityCourses(this.data.user.city_id);
    await this.spinner.hide();
    console.log(this.courses);
  }

  async subscribeForCourse(courseId) {
    await this
      .rest
      .subscribeForCourse({
        course_id: courseId
      }).then((res) => {
        console.log(res);
        this
          .data
          .addToast('Вы успешно записаны на курс', '', 'success');
        this
          .router
          .navigate(['/dashboard']);
      });
  }
}

