import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ListComponent } from './list.component';

import { CoursesRoutingModule } from './courses-routing.module';
import {FormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {MycourseinstancesComponent} from './mycourseinstances.component';
import {CatalogComponent} from './catalog.component';
import {SubscribesComponent} from './subscribes.component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaskModule.forRoot(),
    CoursesRoutingModule,
    FormsModule
  ],
  declarations: [
    ListComponent,
    CatalogComponent,
    SubscribesComponent,
    MycourseinstancesComponent
  ]
})
export class CoursesModule { }
