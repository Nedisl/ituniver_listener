import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import * as moment from 'moment';

@Component({
  templateUrl: 'list.component.html'
})


export class ListComponent implements OnInit {
  courses: any = null;
  availableCourses: any = null;

  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    if (this.courses == null) {
      await this.spinner.show();
    }

    await this.data.getListenerProfile();
    this.courses = await this.rest.getUniversityCourseInstances(this.data.user.university_id);
    this.availableCourses = this.courses.data.course_instances.filter(function (course) {
      return moment(course.apply_end_date) >= moment();
    });
    await this.spinner.hide();
    console.log(this.courses);
  }

  async signUpForCourse(instanceId) {
    await this
      .rest
      .signUpForCourse({
        course_instance_id: instanceId
      }).then((res) => {
        console.log(res);
        this
          .data
          .addToast('Вы успешно записаны на курс', '', 'success');
        this
          .router
          .navigate(['/dashboard']);
      });
  }
}

